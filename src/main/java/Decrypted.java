import java.io.IOException;

import java.io.IOException;
public class Decrypted {

    public static void  handleDecryptData() throws IOException {
        System.out.println("You choose to Decrypt ");
        FileHandle localFileHandle = new FileHandle();
        String decryptFilePath = localFileHandle.FilePath("What is the file name you want to Decrypt ? ");
        boolean fileExists = localFileHandle.FileExists(decryptFilePath);
        if (!fileExists) {
            System.out.println("The Decrypt File doesn't exists ");
            System.exit(1);
        }
        String keyFilePath = localFileHandle.FilePath("What is the key file name ? ");
        fileExists = localFileHandle.FileExists(keyFilePath);
        if (!fileExists) {
            System.out.println("The Key File doesn't exists ");
            System.exit(1);
        }

        String decryptFileData = localFileHandle.FileRead(decryptFilePath);
        String keyFileData = localFileHandle.FileRead(keyFilePath);

        //read the key
        String decrpytedData = localFileHandle.FileReplaceData(decryptFileData,keyFileData,"");

        StringHandle locLStringHandle = new StringHandle();
        String newFileName = locLStringHandle.ReplaceData(decryptFilePath, "_encrypted" ,"_decrypted");
        //Create the  decrypt file name

        localFileHandle.FileWrite(newFileName,  decrpytedData);
        System.out.println("The Decrypted file name is: " + newFileName);

        System.exit(0);


    }
}
