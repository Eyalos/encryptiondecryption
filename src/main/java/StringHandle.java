import java.io.IOException;

public class StringHandle {

    public char[] splitToaArray(String stringToRead) throws IOException {
        char[] splitChar = stringToRead.toCharArray();
        return splitChar;

    }

    public String ReplaceData (String dataSource, String dataToReplace, String replaceWith) {
        if (dataSource.length() > 0)
        {
            String dataToReturn = dataSource.replace(dataToReplace, replaceWith);
            return dataToReturn;
        }else
        {
            return dataSource;
        }
    }
}
